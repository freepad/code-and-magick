'use strict';

function getRandom(n) {
  return Math.round(Math.random() * n);
}

function getRandomElement(arr) {
  return arr[getRandom(arr.length - 1)];
}

var setupNode = document.querySelector('.setup');
setupNode.classList.remove('hidden');
var NAMES = ['Иван', 'Хуан Себастьян', 'Мария', 'Кристоф', 'Виктор', 'Юлия', 'Люпита', 'Вашингтон'];
var SURNAMES = ['да Марья', 'Верон', 'Мирабелла', 'Вальц', 'Онопко', 'Топольницкая', 'Нионго', 'Ирвинг'];
var COLORS = ['rgb(101, 137, 164)', 'rgb(241, 43, 107)', 'rgb(146, 100, 161)', 'rgb(56, 159, 117)', 'rgb(215, 210, 55)', 'rgb(0, 0, 0)'];
var EYE_COLORS = ['black', 'red', 'blue', 'yellow', 'green'];
var PLAYERS_COUNT = 4;
var players = [];

for (var i = 0; i < PLAYERS_COUNT; i++) {
  var player = {
    name: getRandomElement(NAMES) + ' ' + getRandomElement(SURNAMES),
    coatColor: getRandomElement(COLORS),
    eyesColor: getRandomElement(EYE_COLORS)
  };
  players[i] = player;
}
var similarListElement = document.querySelector('.setup-similar-list');
var similarWizardTemplate = document.querySelector('#similar-wizard-template')
    .content
    .querySelector('.setup-similar-item');

for (i = 0; i < PLAYERS_COUNT; i++) {
  var wizardElement = similarWizardTemplate.cloneNode(true);
  wizardElement.querySelector('.setup-similar-label').textContent = players[i].name;
  wizardElement.querySelector('.wizard-coat').style.fill = players[i].coatColor;
  wizardElement.querySelector('.wizard-eyes').style.fill = players[i].eyesColor;
  similarListElement.appendChild(wizardElement);
}

var similarNode = document.querySelector('.setup-similar');
similarNode.classList.remove('hidden');
