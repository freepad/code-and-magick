'use strict';

window.renderStatistics = function (ctx, names, times) {
  var MAX_COL_HEIGHT = 150;
  var COL_WIDTH = 40;
  var MARGIN_LEFT = 120;
  var COL_MARGIN = 50;
  var MAX_SCORE = Math.max.apply(null, times);

  ctx.fillStyle = 'black';
  ctx.fillRect(110, 20, 420, 270);
  ctx.fillStyle = 'white';
  ctx.fillRect(100, 10, 420, 270);
  ctx.font = '16px PT Mono';
  ctx.fillStyle = 'black';
  ctx.fillText('Ура вы победили!', 120, 45);
  ctx.fillText('Список результатов:', 120, 65);

  for (var i = 0; i < names.length; i++) {
    ctx.fillStyle = 'black';
    ctx.fillText(Math.round(times[i]), MARGIN_LEFT, 90);
    ctx.fillText(names[i], MARGIN_LEFT, 270);
    var randomPercent = Math.random() * 100;

    if (names[i] === 'Вы') {
      ctx.fillStyle = 'rgba(255, 0, 0, 1)';
    } else {
      ctx.fillStyle = 'hsl(240, ' + randomPercent + '%, 50%)';
    }

    var colHeight = MAX_COL_HEIGHT / MAX_SCORE * times[i];
    var marginTop = 100 + (MAX_COL_HEIGHT - colHeight);
    ctx.fillRect(MARGIN_LEFT, marginTop, COL_WIDTH, colHeight);
    MARGIN_LEFT = MARGIN_LEFT + COL_WIDTH + COL_MARGIN;
  }
};
